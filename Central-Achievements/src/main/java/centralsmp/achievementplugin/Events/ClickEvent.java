package centralsmp.achievementplugin.Events;

import centralsmp.achievementplugin.AchievementPlugin;
import centralsmp.achievementplugin.AchievementPlugin.MenuItemData;
import centralsmp.achievementplugin.AchievementPlugin.MenuTitle;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataContainer;

import java.util.Objects;

public class ClickEvent implements Listener {
    @EventHandler
    public void clickEvent(InventoryClickEvent e) {
        Player player = (Player) e.getWhoClicked();
        //Main Menu Clicked
        if(e.getView().getTitle().equals(MenuTitle.MAIN.title)
                && e.getCurrentItem() != null && e.getCurrentItem().getItemMeta() != null) {
            e.setCancelled(true);
            PersistentDataContainer playerData = player.getPersistentDataContainer();
            MenuItemData menuItem = MenuItemData.getItemData(e.getCurrentItem().getItemMeta().getLocalizedName());
            if(menuItem == null)
                return;
            switch(menuItem){
                case ACHIEVEMENTS:
                    player.performCommand("me 'is trying to look at achievements'");
                    MenuItemData[] items = new MenuItemData[6];
                    MenuItemData[] routes = AchievementPlugin.routeMenu;
                    int added = 0;
                    for (MenuItemData route : routes) {
                        if (AchievementPlugin.getBoolData(playerData, route.refName)) {
                            items[added] = route;
                            added++;
                        }
                    }
                    //TODO Make it so the data to access these menu items gets created!!!
                    ItemStack[] achievementMenu = AchievementPlugin.buildMenuItems(items);

                    //Here we need to be able to check what achievements are available for the player,
                    // and the player's completed achievements.

                    Objects.requireNonNull(e.getClickedInventory()).setContents(achievementMenu);
                    break;
                case SHOP:
                    player.sendMessage("Clicked Emerald");
                    break;
                case HELP:
                    player.sendMessage("Requesting Help");
                    player.performCommand("help");
                    player.closeInventory();
                    break;
                default:
                    player.closeInventory();
                    break;

            }
        }
    }

}
