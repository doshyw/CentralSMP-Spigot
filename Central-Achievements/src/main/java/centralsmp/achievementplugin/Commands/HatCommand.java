package centralsmp.achievementplugin.Commands;

import centralsmp.achievementplugin.AchievementPlugin;
import centralsmp.achievementplugin.AchievementPlugin.MenuTitle;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class HatCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(commandSender instanceof Player){
            Player player = (Player) commandSender;
            
            ItemStack held = player.getInventory().getItemInMainHand();
            ItemStack helmet = player.getInventory().getHelmet();
            
            player.getInventory().setHelmet(held);
            player.getInventory().setItemInMainHand(helmet);
        }

        return true;
    }
}
