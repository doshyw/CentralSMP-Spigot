package centralsmp.achievementplugin.Events;

import centralsmp.achievementplugin.AchievementPlugin;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.advancement.AdvancementProgress;
import org.bukkit.block.Biome;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.persistence.PersistentDataContainer;

public class MoveEvent implements Listener{

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        Player player = e.getPlayer();
        PersistentDataContainer pdc = player.getPersistentDataContainer();
        Biome biome = player.getWorld().getBiome(player.getLocation());
        switch(biome){
            case SWAMP:
            case MANGROVE_SWAMP:
                AchievementPlugin.attemptCompletion(player, AchievementPlugin.CustomAchievement.SWAMP_OR_DARK);
                if(AchievementPlugin.getBoolData(pdc, "visitedDark")) {
                    AchievementPlugin.attemptCompletion(player, AchievementPlugin.CustomAchievement.SWAMP_AND_DARK);
                }
                AchievementPlugin.setBoolData(pdc, "visitedSwamp", true);
                break;
            case DARK_FOREST:
                AchievementPlugin.attemptCompletion(player, AchievementPlugin.CustomAchievement.SWAMP_OR_DARK);
                if(AchievementPlugin.getBoolData(pdc, "visitedSwamp")) {
                    AchievementPlugin.attemptCompletion(player, AchievementPlugin.CustomAchievement.SWAMP_AND_DARK);
                }
                AchievementPlugin.setBoolData(pdc, "visitedDark", true);
                break;
        }
    }
}
