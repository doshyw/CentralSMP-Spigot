package centralsmp.achievementplugin.Commands;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.command.*;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class LocateCommand implements TabExecutor
{

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        
        if(strings.length > 0)
        {
            if(commandSender instanceof Player)
            {
                Player player = (Player) commandSender;
        
                boolean wasOp = player.isOp();
        
                try
                {
                    if(!wasOp)
                    {
                        player.setOp(true);
                    }
                    Bukkit.getServer().dispatchCommand(player, "locate biome " + strings[0]);
                }
                finally
                {
                    player.setOp(wasOp);
                }
            }
        }

        return true;
    }
    
    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings)
    {
        if(strings.length == 1)
        {
            Biome[] defaultVals = Biome.values();
            ArrayList<String> output = new ArrayList<String>();
            for(int i = 0; i < defaultVals.length; i++)
            {
                if(!defaultVals[i].name().equals(Biome.CUSTOM.name()))
                {
                    output.add("minecraft:" + defaultVals[i].name().toLowerCase());
                }
            }
            return output;
        }
        return null;
    }
}
