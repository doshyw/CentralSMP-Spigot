package centralsmp.achievementplugin.Commands;

import centralsmp.achievementplugin.AchievementPlugin;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.advancement.AdvancementProgress;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.persistence.PersistentDataType;

public class RouteCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof Player){
            if(strings.length == 0) {
                AdvancementProgress ap = ((Player) commandSender).getAdvancementProgress(Bukkit.getAdvancement(NamespacedKey.fromString("adventure:root")));
                ap.awardCriteria("Impossible");
            } else {
                AdvancementProgress ap = ((Player) commandSender).getAdvancementProgress(Bukkit.getAdvancement(NamespacedKey.fromString("adventure:enterswamp")));
            }
        }
        return true;
    }
}
