package centralsmp.achievementplugin.Events;

import centralsmp.achievementplugin.AchievementPlugin;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.persistence.PersistentDataContainer;

public class PlayerPickupEvent implements Listener{

    @EventHandler
    public void onPlayerPickup(EntityPickupItemEvent e) {
        Player player = e.getEntity() instanceof  Player ? (Player) e.getEntity() : null;
        if(player == null) {
            return;
        }
        PersistentDataContainer pdc = player.getPersistentDataContainer();
        switch (e.getItem().getItemStack().getType()){
            case RED_MUSHROOM:
                AchievementPlugin.setBoolData(pdc, "redShroom", true);
                if(AchievementPlugin.getBoolData(pdc, "brownShroom")){
                    AchievementPlugin.attemptCompletion(player, AchievementPlugin.CustomAchievement.COLLECT_MUSHROOMS);
                }
                break;
            case BROWN_MUSHROOM:
                AchievementPlugin.setBoolData(pdc, "brownShroom", true);
                if(AchievementPlugin.getBoolData(pdc, "redShroom")){
                    AchievementPlugin.attemptCompletion(player, AchievementPlugin.CustomAchievement.COLLECT_MUSHROOMS);
                }
                break;
        }
        if(e.getItem().getItemStack().getType() == Material.RED_MUSHROOM)
            AchievementPlugin.setBoolData(pdc, "redShroom", true);
    }
}
