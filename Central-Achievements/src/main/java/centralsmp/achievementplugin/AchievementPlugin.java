package centralsmp.achievementplugin;

import centralsmp.achievementplugin.Commands.GUICommand;
import centralsmp.achievementplugin.Commands.HatCommand;
import centralsmp.achievementplugin.Commands.LocateCommand;
import centralsmp.achievementplugin.Commands.RouteCommand;
import centralsmp.achievementplugin.Events.ClickEvent;
import centralsmp.achievementplugin.Events.JoinEvent;
import centralsmp.achievementplugin.Events.MoveEvent;
import centralsmp.achievementplugin.Events.PlayerPickupEvent;
import org.bukkit.*;
import org.bukkit.advancement.AdvancementProgress;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Objects;

public final class AchievementPlugin extends JavaPlugin {

    //This is an enum containing data for all possible items that appear in Menus.
    //They can be reused in multiple menus (ie a back button may appear in multiple menus)
    //Their location will be defined by other arrays defining the locations of menu items within a menu of a given title (menu object?)

    public enum MenuTitle {
        MAIN("Main Menu"),
        ROUTES("Achievement Routes");

        public final String title;
        MenuTitle(String title){
            this.title = title;
        }
    }

    public enum CustomAchievement {
        ROOT("swamp",null, "root", 0),
        SWAMP_OR_DARK("swamp", CustomAchievement.ROOT, "enterdarkorswamp", 15),
        SWAMP_AND_DARK("swamp", CustomAchievement.SWAMP_OR_DARK, "enterdarkandswamp", 15),
        SWAMP_50_DAYS("swamp", CustomAchievement.SWAMP_OR_DARK, "swamp50", 15),
        COLLECT_MUSHROOMS("swamp", CustomAchievement.SWAMP_OR_DARK, "getmushrooms", 15),
        CREATE_STEW("swamp", CustomAchievement.COLLECT_MUSHROOMS, "makemushroomstew", 15),
        CREATE_SUSSY_STEW("swamp", CustomAchievement.CREATE_STEW, "makesusstew", 15),
        CREATE_RABBIT_STEW("swamp", CustomAchievement.CREATE_SUSSY_STEW, "makerabbitstew", 15),
        GROW_GIANT_MUSHROOMS("swamp", CustomAchievement.CREATE_RABBIT_STEW, "growmushrooms", 30),
        FERMENT_SPIDER_EYES("swamp", CustomAchievement.GROW_GIANT_MUSHROOMS, "fermentedeyes", 30),
        COLLECT_PODZOL("swamp", CustomAchievement.FERMENT_SPIDER_EYES, "getpodzol", 30),
        COLLECT_MYCELIUM("swamp", CustomAchievement.COLLECT_PODZOL, "getmycelium", 60),
        ENTER_NETHER("swamp", CustomAchievement.ROOT, "nether", 5),
        COLLECT_FUNGUS("swamp", CustomAchievement.ENTER_NETHER, "getwarped", 10),
        POT_FUNGI("swamp", CustomAchievement.COLLECT_FUNGUS, "collectfungi", 15),
        GROW_GIANT_FUNGUS("swamp", CustomAchievement.POT_FUNGI, "grownetherfungi", 15),
        COLLECT_SHROOMLIGHT("swamp", CustomAchievement.GROW_GIANT_FUNGUS, "collectshroomlight", 60),
        TAME_STRIDER("swamp", CustomAchievement.COLLECT_SHROOMLIGHT, "tamestrider", 60),
        FIND_MUSHROOM_FIELDS("swamp", CustomAchievement.ROOT, "findheaven", 60),
        MILK_MOOSHROOM("swamp", CustomAchievement.FIND_MUSHROOM_FIELDS, "milkmooshroom", 15),
        BREED_MOOSHROOM("swamp", CustomAchievement.MILK_MOOSHROOM, "breedmooshroom", 15),
        THUNDER_COW("swamp", CustomAchievement.BREED_MOOSHROOM, "thundercow", 90);

        public String nameSpace;
        public CustomAchievement prereq;
        public String refName;
        public int prize;

        CustomAchievement(String nameSpace, CustomAchievement prereq, String refName, int prize){
            this.nameSpace = nameSpace;
            this.prereq = prereq;
            this.refName = refName;
            this.prize = prize;
        }
    }

    public enum MenuItemData {
        //Main Menu
        ACHIEVEMENTS(ChatColor.GREEN + "View Achievements", "achMenu", "View your Achievements", Material.CRAFTING_TABLE),
        SHOP(ChatColor.GOLD + "Shop", "shop", "Spend your achievement points", Material.EMERALD),
        HELP(ChatColor.BLACK + "Help", "help", "Get help", Material.FURNACE),
        //Achievement Routes
        SWAMP("Questionable Morels", "swamp", "swamp lore", Material.MUD),
        JUNGLE("Beyond the Trees, Above the Clouds", "jungle", "jungle lore", Material.BAMBOO);
        public final String displayName;
        public final String refName;
        public final String lore;
        public final Material material;

        MenuItemData(String displayName, String refName, String lore, Material material){
            this.displayName = displayName;
            this.refName = refName;
            this.lore = lore;
            this.material = material;
        }

        public static MenuItemData getItemData(String refName){
            for(MenuItemData item : MenuItemData.values()){
                if(item.refName.equals(refName))
                    return item;
            }
            return null;
        }
    };

    public static Plugin plugin;
    public static AchievementPlugin achievementController;
    public static MenuItemData[] mainMenu = new MenuItemData[] {
            MenuItemData.ACHIEVEMENTS,
            MenuItemData.SHOP,
            MenuItemData.HELP
    };
    public static MenuItemData[] routeMenu = new MenuItemData[] {
            MenuItemData.SWAMP,
            MenuItemData.JUNGLE
    };

    @Override
    public void onEnable() {
        // Plugin startup logic
        plugin = this;
        achievementController = this;
        getCommand("hat").setExecutor(new HatCommand());
        getCommand("biome").setExecutor(new LocateCommand());
        getCommand("biome").setTabCompleter(new LocateCommand());
        getCommand("gui").setExecutor(new GUICommand());
        getCommand("getRoute").setExecutor(new RouteCommand());
        getServer().getPluginManager().registerEvents(new ClickEvent(), this);
        getServer().getPluginManager().registerEvents(new JoinEvent(), this);
        getServer().getPluginManager().registerEvents(new MoveEvent(), this);
        getServer().getPluginManager().registerEvents(new PlayerPickupEvent(), this);
    }

    public static Plugin getPlugin(){
        return plugin;
    }

    public static ItemStack[] buildMenuItems(MenuItemData[] items) {
        ItemStack[] menuStack = new ItemStack[items.length];
        for (int i = 0; i < items.length; i++) {
            MenuItemData item = items[i];
            if(item != null) {
                ItemStack button = new ItemStack(item.material);
                ItemMeta buttonMeta = button.getItemMeta();
                buttonMeta.setDisplayName(item.displayName);
                buttonMeta.setLocalizedName(item.refName);
                ArrayList<String> itemLore = new ArrayList<String>();
                itemLore.add(item.lore);
                buttonMeta.setLore(itemLore);
                button.setItemMeta(buttonMeta);
                menuStack[i] = button;
            } else {
                //If null, just add air
                menuStack[i] = new ItemStack(Material.AIR);
            }
        }
        return menuStack;
    }

    public static boolean getBoolData(PersistentDataContainer pdc, String keyString){
        return pdc.has(new NamespacedKey(AchievementPlugin.getPlugin(), keyString), PersistentDataType.BYTE);
    }
    public static void setBoolData(PersistentDataContainer pdc, String keyString, boolean data){
        if(data) {
            pdc.set(new NamespacedKey(AchievementPlugin.getPlugin(), keyString), PersistentDataType.BYTE, (byte) 1);
        } else {
            pdc.remove(new NamespacedKey(AchievementPlugin.getPlugin(), keyString));
        }
    }

    public static void awardAdvancement(Player player, CustomAchievement achievement){
        AdvancementProgress ap = player.getAdvancementProgress(Objects.requireNonNull(Bukkit.getAdvancement(Objects.requireNonNull(NamespacedKey.fromString(achievement.nameSpace + ":" + achievement.refName)))));
        ap.awardCriteria("Impossible");
        Firework firework = player.getWorld().spawn(player.getLocation(), Firework.class);
        FireworkMeta fMeta = firework.getFireworkMeta();
        fMeta.addEffect(FireworkEffect.builder().withColor(Color.YELLOW).with(FireworkEffect.Type.CREEPER).build());
        fMeta.setPower(1);
        firework.setFireworkMeta(fMeta);
    };

    public static boolean playerHasCompleted(Player player, CustomAchievement achievement){
        AdvancementProgress ap = player.getAdvancementProgress(Objects.requireNonNull(Bukkit.getAdvancement(Objects.requireNonNull(NamespacedKey.fromString(achievement.nameSpace + ":" + achievement.refName)))));
        return ap.isDone();
    }

    public static boolean attemptCompletion(Player player, CustomAchievement achievement){
        if(!playerHasCompleted(player, achievement)
                && (achievement.prereq == null || playerHasCompleted(player, achievement.prereq))){
            awardAdvancement(player, achievement);
            return true;
        }
        return false;
    }
}
