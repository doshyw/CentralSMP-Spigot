package centralsmp.achievementplugin.Events;

import centralsmp.achievementplugin.AchievementPlugin;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.persistence.PersistentDataContainer;

public class JoinEvent implements Listener{

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {

        Player player = e.getPlayer();
        PersistentDataContainer pdc = player.getPersistentDataContainer();
        if(AchievementPlugin.getBoolData(pdc, "joined")){
            e.setJoinMessage("Welcome back " + player.getDisplayName());
            AchievementPlugin.setBoolData(pdc, "visitedSwamp", false);
            AchievementPlugin.setBoolData(pdc, "visitedDark", false);
            AchievementPlugin.setBoolData(pdc, "redShroom", false);
            AchievementPlugin.setBoolData(pdc, "brownShroom", false);
        } else {
            AchievementPlugin.setBoolData(pdc, "joined", true);
        }
    }
}
