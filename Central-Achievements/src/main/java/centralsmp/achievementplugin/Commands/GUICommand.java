package centralsmp.achievementplugin.Commands;

import centralsmp.achievementplugin.AchievementPlugin;
import centralsmp.achievementplugin.AchievementPlugin.MenuTitle;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class GUICommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(commandSender instanceof Player){
            Player player = (Player) commandSender;

            Inventory gui = Bukkit.createInventory(player, 9, MenuTitle.MAIN.title);
            gui.setContents(AchievementPlugin.buildMenuItems(AchievementPlugin.mainMenu));

            player.openInventory(gui);
        }

        return true;
    }
}
